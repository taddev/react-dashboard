import React from 'react';

class Home extends React.Component {
    componentDidMount = () => {
        document.title = "Home";
    }

    render() {
        return <h1 className="title">Home</h1>;
    }
}

export default Home;
