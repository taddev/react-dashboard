import React, { useState, useEffect, useRef } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import IdleTimer from 'react-idle-timer';
import format from 'date-fns/format';
import 'bulma/css/bulma.css';
import './App.css';
import Navigation from './Navigation';
import SideMenu from './SideMenu';
import Home from './Home';
import DataTable from './Table';
import PageButton from './PageButton';
import TreeInput from './TreeInput';
import Idler from './Idler';

function App() {
    const idleTimer = useRef();
    const [timeout] = useState(3000);
    const [remaining, setRemaining] = useState(null);
    const [isIdle, setIdle] = useState(false);
    const [lastActive, setLastActive] = useState(null);
    const [elapsed, setElapsed] = useState(null);
    const [loaded, setLoaded] = useState(false);

    const onActive = () => {
        setIdle(false);
    }

    const onIdle = () => {
        setIdle(true);
    }

    // const changeTimeout = () => {
    //     this.setState({
    //         timeout: refs.timeoutInput.state.value()
    //     })
    // }

    const reset = () => {
        idleTimer.reset()
    }

    const pause = () => {
        idleTimer.pause()
    }

    const resume = () => {
        idleTimer.resume()
    }

    useEffect(() => {
        setRemaining(idleTimer.current.getRemainingTime());
        setLastActive(idleTimer.current.getLastActiveTime());
        setElapsed(idleTimer.current.getElapsedTime());

        if (!loaded) {
            setInterval(() => {
                setRemaining(idleTimer.current.getRemainingTime());
                setLastActive(idleTimer.current.getLastActiveTime());
                setElapsed(idleTimer.current.getElapsedTime());
            }, 1000);
            setLoaded(true);
        }
    }, [idleTimer, loaded]);

    // componentDidMount() {
    //     this.setState({
    //         remaining: this.idleTimer.getRemainingTime(),
    //         lastActive: this.idleTimer.getLastActiveTime(),
    //         elapsed: this.idleTimer.getElapsedTime()
    //     })

    //     setInterval(() => {
    //         this.setState({
    //             remaining: this.idleTimer.getRemainingTime(),
    //             lastActive: this.idleTimer.getLastActiveTime(),
    //             elapsed: this.idleTimer.getElapsedTime()
    //         })
    //     }, 1000)
    // }

    // render() {
    return (
        <Router basename={process.env.PUBLIC_URL}>
            <IdleTimer
                ref={idleTimer}
                onActive={onActive}
                onIdle={onIdle}
                timeout={timeout}
                startOnMount
            />
            <Navigation />
            <div className="columns is-gapless is-fullheight">
                <div className="column is-2 has-border-right">
                    <SideMenu />
                </div>
                <div className="column">
                    <div className="section">
                        <div className="container">
                            <h1 className="title">Idler</h1>
                            <div>
                                <h1>Timeout: {timeout}ms</h1>
                                <h1>Time Remaining: {remaining}</h1>
                                <h1>Time Elapsed: {elapsed}</h1>
                                <h1>Last Active: {format(lastActive, 'MM-DD-YYYY HH:MM:ss.SSS')}</h1>
                                <h1>Idle: {isIdle.toString()}</h1>
                            </div>
                            <div>
                                <button onClick={reset}>RESET</button>
                                <button onClick={pause}>PAUSE</button>
                                <button onClick={resume}>RESUME</button>
                            </div>
                        </div >
                    </div>
                    <main className="container">
                        <Route path="/" exact component={Home} />
                        <Route path="/button" component={PageButton} />
                        <Route path="/tree" component={TreeInput} />
                        <Route path="/table" component={DataTable} />
                        <Route path="/idler/" component={Idler} />
                    </main>
                </div>
            </div>
        </Router>
    );
}

export default App;
