import React from 'react';
import { NavLink } from 'react-router-dom';

function SideMenu() {
    return (
        <aside className="menu">
            <p className="menu-label">
                General
        </p>
            <ul className="menu-list">
                <li><NavLink to="/">Home</NavLink></li>
                <li><NavLink to="/button">Button</NavLink></li>
                <li><NavLink to="/tree">Trees</NavLink></li>
                <li><NavLink to="/table">Table</NavLink></li>
                <li><NavLink to="/idler">Idler</NavLink></li>
            </ul>
        </aside>
    );
}

export default SideMenu;
