import React from 'react';
import { Link } from 'react-router-dom';

function Navigation() {
    return (
        <nav className="navbar has-shadow">
            <div className="navbar-menu">
                <div className="navbar-start">
                    <Link to="/" className="navbar-item">Home</Link>
                    <Link to="/button" className="navbar-item">Button</Link>
                    <Link to="/tree" className="navbar-item">Trees</Link>
                    <Link to="/table" className="navbar-item">Table</Link>
                    <Link to="/idler" className="navbar-item">Idler</Link>
                </div>
            </div>
        </nav>
    );
}

export default Navigation;
