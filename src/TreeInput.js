import React, { useEffect } from 'react';

function TreeInput() {
    const [tree, setTree] = React.useState("oak");
    const handleTreeChange = event => {
        setTree(event.target.value);
    }

    useEffect(() => {
        document.title = "Trees";
    });

    return (
        <div className="field">
            <label className="label">Trees</label>
            <div className="control">
                <input
                    className="input"
                    type="text"
                    placeholder="Text input"
                    value={tree}
                    onChange={handleTreeChange}
                />
            </div>
            <p className="help">You're requesting a {tree} tree.</p>
        </div>
    );
}

export default TreeInput;
