import React, { useEffect } from 'react';


function PageButton() {
    const [count, setCount] = React.useState(0);

    useEffect(() => {
        document.title = "Button"
    });

    return (
        <React.Fragment>
            <h1 className="title">Button</h1>
            <button
                className="button is-primary"
                onClick={() => { setCount(count + 1) }}
            >
                Clicks {count}
            </button>
        </React.Fragment>
    );
}

export default PageButton;
